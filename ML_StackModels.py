# NIST-developed software is provided by NIST as a public service. You may use, copy and distribute copies of the software in any medium, provided that you keep intact this entire notice. You may improve, modify and create derivative works of the software or any portion of the software, and you may copy and distribute such modifications or works. Modified works should carry a notice stating that you changed the software and should note the date and nature of any such change. Please explicitly acknowledge the National Institute of Standards and Technology as the source of the software.

# NIST-developed software is expressly provided "AS IS." NIST MAKES NO WARRANTY OF ANY KIND, EXPRESS, IMPLIED, IN FACT OR ARISING BY OPERATION OF LAW, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT AND DATA ACCURACY. NIST NEITHER REPRESENTS NOR WARRANTS THAT THE OPERATION OF THE SOFTWARE WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ANY DEFECTS WILL BE CORRECTED. NIST DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OF THE SOFTWARE OR THE RESULTS THEREOF, INCLUDING BUT NOT LIMITED TO THE CORRECTNESS, ACCURACY, RELIABILITY, OR USEFULNESS OF THE SOFTWARE.

# You are solely responsible for determining the appropriateness of using and distributing the software and you assume all risks associated with its use, including but not limited to the risks and costs of program errors, compliance with applicable laws, damage to or loss of data, programs or equipment, and the unavailability or interruption of operation. This software is not intended to be used in any situation where a failure could cause risk of injury or damage to property. The software developed by NIST employees is not subject to copyright protection within the United States.

from sklearn import datasets
from sklearn.tree import DecisionTreeClassifier,DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
from sklearn.neighbors import KNeighborsClassifier,KNeighborsRegressor
from sklearn.linear_model import LogisticRegression,LinearRegression
from sklearn.ensemble import StackingClassifier,StackingRegressor
from sklearn.ensemble import GradientBoostingClassifier,GradientBoostingRegressor
import xgboost
import pandas as pd
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
import sys
import numpy as np
from sklearn.model_selection import RandomizedSearchCV
from tdc.benchmark_group import admet_group
from config import  best_params
from config import *
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, LSTM, Dense
from tensorflow.keras import Sequential
from tensorflow.keras import optimizers
import argparse
#import tensorflow.keras
from sklearn.metrics import classification_report, confusion_matrix
import random
from tensorflow.keras.layers import *
from tdc import utils
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.metrics import mean_absolute_error,r2_score,mean_squared_error
from sklearn import random_projection
import csv,os
from sklearn.svm import SVR
PATH="../data/"

def Save_Results_csv(filename,taskname,cls,mean_std):

#    maccskeys,circular,mol2vec,mordred,rdkit):
    filename=filename+'.csv'
    if not os.path.exists(filename):
        file = open(filename, 'w+', newline ='')
        header = ['task name', 'mean','std','classifier']#'maccskeys','circular','mol2vec','mordred','rdkit','Weight1-RF']
        writer = csv.DictWriter(file, fieldnames = header)
        writer.writeheader()
        writer.writerow({'task name':taskname, 'mean':mean_std[0],'std':mean_std[1],'classifier':cls})
        #'maccskeys':feature_imp[0],'circular':feature_imp[1],'mol2vec':feature_imp[2],'mordred':feature_imp[3],'rdkit':feature_imp[4],'Weight1-RF':weight})

    else:
        file = open(filename, 'a+', newline ='')
        header = ['task name', 'mean','std','classifier']#'maccskeys','circular','mol2vec','mordred','rdkit','Weight1-RF']
        writer = csv.DictWriter(file, fieldnames = header)
        writer.writerow({'task name':taskname, 'mean':mean_std[0],'std':mean_std[1],'classifier':cls})
        #'maccskeys':feature_imp[0],'circular':feature_imp[1],'mol2vec':feature_imp[2],'mordred':feature_imp[3],'rdkit':feature_imp[4],'Weight1-RF':weight})


def Prepare_Data(name,benchmark):

    train_val, test = benchmark['train_val'], benchmark['test']
    y_train_val = train_val.iloc[:, 2].tolist()
    test=test.iloc[:,2].tolist()
    fp_train_val = np.load(open(PATH + name + "_train_val.npy", "rb"))
    fp_test = np.load(open(PATH + name + "_test.npy", "rb"))
    fp_train_val=np.array(fp_train_val)
    fp_test=np.array(fp_test)
    test=np.array(test)
    y_train_val=np.array(y_train_val)
    print('SHAPE OF TRAIN AND TEST FETAURES',fp_train_val.shape,np.array(y_train_val).shape,fp_test.shape,np.array(test).shape)
    return fp_train_val,y_train_val,fp_test,test#tra

def UniModel(train_feat,train_label,random_state,best_par,model_type):

    if(model_type=='gbt'):
        clf = GradientBoostingRegressor(random_state=random_state)
    elif(model_type=='dtc'):
        clf =  DecisionTreeRegressor( random_state=random_state)
    elif(model_type=='rfc'):
        clf = RandomForestRegressor( random_state=random_state)
    elif(model_type=='knn'):
        clf =KNeighborsRegressor()
    elif(model_type=='svr'):
        clf=SVR(C=1.0, epsilon=0.2)
    else:
        clf = xgboost.XGBRegressor( random_state=random_state)
    
    clf.fit(train_feat,train_label)
    return clf
#Stacking

def Model_Predict(model,fp_test):

    pred = model.predict(fp_test)
    #pred_prob=0#model.predict_prob(feat_test)
    #feature_imp=model.feature_importances_

    return pred #, pred_prob,feature_imp

def Stack_Classify():
    dtc =  DecisionTreeClassifier()
    rfc = RandomForestClassifier()
    knn =  KNeighborsClassifier()
    xgb = xgboost.XGBClassifier()
    gbt = GradientBoostingRegressor(random_state=random_state)
    svr=SVR(C=1.0, epsilon=0.2)
    clf = [(gbt,'gbt'),('dtc',dtc),('rfc',rfc),('knn',knn),('xgb',xgb),('svr',svr)] #list of (str, estimator
    lr = LogisticRegression()
    stack_model = StackingClassifier( estimators = clf,final_estimator = lr)
    score = cross_val_score(stack_model,X,y,cv = 5,scoring = 'accuracy')

def Multimodal(train_feat,train_label,random_state,best_par):

    dtc= DecisionTreeRegressor( random_state=random_state)
    rfc=RandomForestRegressor( random_state=random_state)
    knn=KNeighborsRegressor()
    xgb=xgboost.XGBRegressor( tree_method='gpu_hist', **best_par,random_state=random_state)
    gbt= GradientBoostingRegressor(random_state=random_state)
    svr=SVR(C=1.0, epsilon=0.2)

    clf = [(gbt,'gbt'),('dtc',dtc),('rfc',rfc),('knn',knn),('xgb',xgb),('svr',svr)] #list of (str, estimator
    lr = LinearRegression()
    stack_model=StackingRegressor(estimators = clf,final_estimator = lr)
    stack_model.fit(train_feat,train_label)
    return stack_model

def Multimodal1(train_feat,train_label,random_state,best_par):
    dtc= DecisionTreeRegressor( random_state=random_state)
    rfc=RandomForestRegressor( random_state=random_state)
    knn=KNeighborsRegressor()
    xgb=xgboost.XGBRegressor( tree_method='gpu_hist', **best_par,random_state=random_state)
    clf = [('dtc',dtc),('rfc',rfc),('knn',knn),('xgb',xgb)] #list of (str, estimator
    lr = LinearRegression()
    stack_model=StackingRegressor(estimators = clf,final_estimator = xgb)
    stack_model.fit(train_feat,train_label)
    return stack_model


def Evaluate_Results(predictions_list):
    result=group.evaluate_many(predictions_list)
    return result


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--filename',type=str, default='StackModel',help='filename to save model output results')
    parser.add_argument('--unimodal',type=int, default=1,help='1-unimodal, 0-multimodal')
    arguments = parser.parse_args()
    filename=arguments.filename
    unimodal=arguments.unimodal
    model_type=['svr','tdc','rfc','knn','xgb','gbt']
    #tasks=['cyp3a4_veith','cyp2c9_substrate_carbonmangels', 'cyp2d6_substrate_carbonmangels','cyp3a4_substrate_carbonmangels']
    if(unimodal==1):
        model_type=['svr','tdc','rfc','knn','xgb']
        for type_model in model_type:
            print('Model', type_model)
            for task_name in list_tasks_binary:
                group = admet_group(path='data/')
                print('TASK is',task_name)
                benchmark = group.get(task_name)#sys.argv[1])
                name = benchmark['name']
                [train_feat,train_label,test_feat,test_label]=Prepare_Data(name,benchmark)
                best_par=best_params[name]#Fine_tune_parameters(train_feat,train_label,name,n_iter=iterations,model_type=modeltype)
                predictions_list,feature_imp_list,predictions_prob_list=[],[],[]
                for random_state in [1,2,3,4,5]:
                    predictions,predictions_prob={},{}
                    model=UniModel(train_feat,train_label,random_state,best_par,type_model)
                    pred=Model_Predict(model,test_feat)
                    predictions[name]=pred
                 #   predictions_prob[name]=pred_prob
                    predictions_list.append(predictions)
                    #feature_imp_list.append(features)
                   # predictions_prob_list.append(pred_prob)

                results=Evaluate_Results(predictions_list) # 2X1 mean std
                results=results[name]
                #feat_imp=Feature_Imp(feature_imp_list) # 5X1
                print(type(results),results)#,type(feat_imp),feat_imp)
                Save_Results_csv(filename,task_name,type_model,results)
    else:
        for task_name in list_tasks_regression:
            group = admet_group(path='data/')
            print('TASK is',task_name)
            benchmark = group.get(task_name)#sys.argv[1])
            name = benchmark['name']
            [train_feat,train_label,test_feat,test_label]=Prepare_Data(name,benchmark)
            best_par=best_params[name]#Fine_tune_parameters(train_feat,train_label,name,n_iter=iterations,model_type=modeltype)
            predictions_list,feature_imp_list,predictions_prob_list=[],[],[]
            for random_state in [1,2,3,4,5]:
                predictions,predictions_prob={},{}
                model=Multimodal(train_feat,train_label,random_state,best_par)
                pred=Model_Predict(model,test_feat)
                predictions[name]=pred
                predictions_list.append(predictions)
                #feature_imp_list.append(features)
                #predictions_prob_list.append(pred_prob)

            results=Evaluate_Results(predictions_list) # 2X1 mean std
            results=results[name]
            #feat_imp=Feature_Imp(feature_imp_list) # 5X1
            print(type(results),results)#,type(feat_imp),feat_imp)
            Save_Results_csv(filename,task_name,'stacked_model',results)

